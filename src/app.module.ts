import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FilesController } from './files.conroller';
import { KeysService } from './keys.service';

@Module({
  imports: [],
  controllers: [AppController, FilesController],
  providers: [AppService, KeysService],
})
export class AppModule { }
