import { Body, Controller, Get, Param, Post, Put, Query } from "@nestjs/common";
import { AESDecrypt, AESEncrypt, staticSecretKey } from "./AES-crypto-methods";
const fs = require('fs')
// import fs from 'fs';


@Controller()
export class FilesController {

    @Get('files')
    listFiles(
    ) {

        const res = fs.readdirSync(__dirname + '/../files-store/')

        return res;
    }


    @Get('files/:fileName')
    getFileByName(
        @Param('fileName') fileName: string,
        @Query('encrypt') encrypt: string,
    ) {

        try {

            let fileContent = fs.readFileSync(__dirname + '/../files-store/' + fileName).toString();

            if (encrypt == 'AES')
                fileContent = AESEncrypt({ key: staticSecretKey, plainPayload: fileContent })


            return {
                fileContent,
            }
        } catch (error) {
            console.error(error);
            return null;
        }
    }


    @Post('files')
    addNewFile(
        @Body() { fileContent, fileName }: { fileContent: string, fileName: string },
        @Query('encrypt') encrypt: string,
    ) {

        try {

            if (encrypt == 'AES') {
                return fs.writeFileSync(__dirname + '/../files-store/' + fileName, AESDecrypt({ key: staticSecretKey, encryptedPayload: fileContent }));
            }

            return fs.writeFileSync(__dirname + '/../files-store/' + fileName, fileContent);
        } catch (error) {
            return error;
        }
    }


    @Put('files/:fileName')
    updateFile(
        @Param('fileName') fileName: string,
        @Body() { fileContent }: { fileContent: string },
        @Query('encrypt') encrypt: string,
    ) {

        try {
            if (encrypt == 'AES') {
                return fs.writeFileSync(__dirname + '/../files-store/' + fileName, AESDecrypt({ key: staticSecretKey, encryptedPayload: fileContent }));
            }
            else {
                return fs.writeFileSync(__dirname + '/../files-store/' + fileName, fileContent);
            }

        } catch (error) {
            return error;
        }
    }
}