import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { KeysService } from './keys.service';
import { RSADecrypt, RSAEncrypt, RSAGenerateKeys, str2ab } from './RSA-crypt-methods';



@Controller()
export class AppController {


  constructor(
    private readonly appService: AppService,
    private keysService: KeysService,
  ) { }



  @Get()
  getHello(): any {
    const plainText = 'Hello World';

    const { privateKey, publicKey } = RSAGenerateKeys();

    console.log({ privateKey, publicKey });
    const encrypted = RSAEncrypt(plainText, publicKey);
    console.log({ encrypted });
    const decrypted = RSADecrypt(encrypted, privateKey);
    console.log({ decrypted })


    return this.appService.getHello();
  }



  @Get('/public-key')
  getPublicKey() {
    // console.log({ publicKey: this.publicKey, privateKey: this.privateKey })
    return { publicKey: this.keysService.getPrivateKey() };
  }




  @Post('dec')
  decrypt(
    @Body() { payload }: { payload: string }
  ) {
    console.log({ payload })
    return RSADecrypt(str2ab((payload)), (this.keysService.getPrivateKey()))
  }
}
