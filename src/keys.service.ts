import { Injectable } from "@nestjs/common";
import { RSAGenerateKeys } from "./RSA-crypt-methods";



@Injectable()
export class KeysService {


    publicKey: string = '';
    privateKey: string = '';

    constructor() {
        const { privateKey, publicKey } = RSAGenerateKeys();
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    getPublicKey() {
        return this.publicKey
    }

    getPrivateKey() {
        return this.privateKey
    }
}